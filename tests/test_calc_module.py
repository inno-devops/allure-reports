import pytest
from src import calc_module

def test_addition():
    assert calc_module.add(1, 2) == 3

def test_subtraction():
    assert calc_module.subtract(5, 2) == 3

def test_multiplication():
    assert calc_module.multiply(2, 3) == 6

def test_division():
    assert calc_module.divide(6, 3) == 2

def test_divide_by_zero():
    with pytest.raises(ZeroDivisionError):
        calc_module.divide(1, 0)
